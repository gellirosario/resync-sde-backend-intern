# Back-end App Test

Back-end application development task. Back-end app should be developed in express.js framework. Database can be used SQL / NoSQL (Postgress, MySQL, MongoDB, InfluxDB, etc). Coding should be done using "camelCase" standard. Each function & business logic should have proper commenting.

**NOTE:** 
- Don't push the code in the master branch. Fork the branch and then start developing the codebase.
- Don't add node_modules directory
- create "backend" directory for Back-End codebase
- create "database" directory for datatabase file
- Nice to have feature - Docker & docker-compose.yml file [ Build and generate artifacts ]
- Add examples of how to use the APIs in your project's README file

**Problem Statement:**
Design and develop a back-end application which should return an Restful API for employee management.

**API List:**
1. `/login` API that checks if the username && password is correct and returns a token, this token must be given in the header of subsequent APIs and validated before returning response
2. `/getEmployee/:e_id` API that provides detail of given employee_id
3. `/getEmployees` API that provides list of all employees

# Solution
## Summary
The back-end application provides API for employee management. It was implemented in Node.js and Express framework. The database used is MongoDB Atlas.
It also uses packages such as:
- express,mongoose,bcrypt,jsonwebtoken,dotnev,body-parser, nodemon 

## Database set up
The database folder contains the .json files that was used for the testing environment. It contains the whole collection.

## Application Set up
1. Clone repo
2. Run npm install and install the packages listed earlier (express,mongoose,bcrypt,jsonwebtoken,dotnev,body-parser, nodemon)
3. Navigate to backend folder by 'cd backend'
4. Run the application by 'npm start' (This will start the application on port 3000)

## Authentication
The authentication is implemented with jsonwebtoken package. After log in, an authentication token will be generated and the token must be provided to use for employee functions.
An error would occur if:
1. Invalid token
2. Expired token
3. Missing token

## API Implementation
1. Register User (/register)
- POST request
- It is required to pass a username and password in the request body as a json object
- Allow user to register 

2. Register Employee (/employee/register)
- POST request
- It is required to pass a e_id, employeeName in the request body as a json object
- Allows authorised user to register an employee with authentication token

3. Login (/login)
- POST request
- Return an authentication token
- It is required to pass a username and password in the request body as a json object
- Allows authorised user to login

4. Get All Employees (/getEmployees)
- GET request
- Retrieves all the employees name and other details

5. Get Employee by ID (/getEmployee/:e_id)
- GET request
- It is required to provide an e_id (EmployeeID)
- Allows authorised user to get employee by id


## API Testing
The API was tested using Talend API Tester. 

**Guide of steps**
1. Add User by making POST Request to "localhost:3000/register" and add json object containing username and password. 
- Example: {"username":"test", "password":"testing"}
2. Login as a User by making POST Request to "localhost:3000/login" and add json object containing username and password. 
- Example: {"username":"test", "password":"testing"}
- Return: authentication token (example: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9eyJ1c2VybmFtZSI6InRlc3QiLCJpZCI6IjYwNWQ2Y2VlOWNhODIwMzlmMGZiMWRhMyIsImlhdCI6MTYxNjczOTMwNywiZXhwIjoxNjE2ODI1NzA3fQ.c5DxhKXRo3cDuUZMYI6kF4hWNBJdD-5zT6lquz0Ubmc)
3. Add Employee by making POST Request to "localhost:3000/employee/register", add json object containing employee id, name and add token header with the value of authentication token.
- Example: {"e_id":"4","employeeName":"Joel Lim"}
4. Get List of Employees by making GET Request to "localhost:3000/getEmployees" and add 'token' header with the value of authentication token.
5. Get Employee by ID by making GET Request to "localhost:3000/getEmployee/:e_id" and add 'token' header with the value of authentication token.
- Example: "http://localhost:3000/getEmployee/1"