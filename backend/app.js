//Import required packages
const express = require('express'); // for creating API
const bodyParser = require('body-parser');
const mongoose = require('mongoose'); // connect to mongodb
const dotenv = require('dotenv'); // load environment variables
dotenv.config();

//Constants
const app = express();

//Import Routes
const loginRoute = require('./routes/login');
const employeeRoute = require('./routes/employee');

//Middlewares
app.use(express.json());
app.use(bodyParser.urlencoded({ extended: false })); // used for request parsing
app.use('/', loginRoute); // map link to routes
app.use('/', employeeRoute); // map link to routes

//Handle unrouted urls
app.all('*', function(req, res) {
    throw new Error("Bad request")
})

app.use(function(e, req, res, next) {
    if (e.message === "Bad request") {
        res.status(404).json({error: {msg: e.message, stack: e.stack}});
    }
});


//Connect to DB
mongoose.connect(process.env.DB_CONNECTION,
    { useNewUrlParser: true },
    () => 
    console.log('Connected to DB')
);

//Listen to the server
app.listen(process.env.PORT, () => console.log(`Backend server started on port ${process.env.PORT}`));