const express = require('express'); // for creating API
const jwt = require('jsonwebtoken'); // for auth
const bcrypt = require('bcrypt');
const router = express.Router();

// Model used
var User = require('../models/User');

//Login API for Employees
// Test link: http://localhost:3000/login
// Param: (Example) 
// {"username":"test",
// "password":"testing"}
router.post('/login', async(req, res) => {

    const username = req.body.username;
    const password = req.body.password;

    // Find if user exist in db
    const user = await User.findOne({username: username}, function (err, user){
        if (err) return res.status(500).send('Error: Error on the server');
        if (!user) return res.status(404).send('Error: No user found');
    })
    
    // Check if password is valid
    const isValid = await bcrypt.compareSync(password, user.password);
    if (!isValid) return res.status(401).send({ auth: false, token: null });
  
    //console.log(`${user}`);

    var token = jwt.sign({ username: user.username, id: user._id }, process.env.TOKEN, {
            expiresIn: 86400 // token will expires in 24 hours
    }, function(err, token) {
        if(err) return res.status(500).send('Error: Error on the server');
        
        //console.log(token);
        // return token
        res.status(200).header('token', token).send(token);
    });
    
    
});

// Register new user (for testing)
// Test link: http://localhost:3000/register
// Param: (Example) 
// {"username":"test",
// "password":"testing"}
router.post("/register", async(req, res)=>{

    // Check if employee exist
    const isExist = await User.findOne({username: req.body.username});
    if (isExist) return res.status(400).send("Error: User already exist in database!");

    // Hash passwords
    const salt = await bcrypt.genSalt(10);
    const hashedPass = await bcrypt.hash(req.body.password, salt)

    // Create new user
    const user = new User({
        username: req.body.username,
        password: hashedPass
    });

    try{
        // Add to database
        const newUser = await user.save();
        res.send({user: newUser.id}); // return new user id
    }
    catch(err){
        res.status(400).send(err);
    }

})


module.exports = router;