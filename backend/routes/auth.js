const express = require('express'); // for creating API
const bodyParser = require('body-parser'); // parse URL and obj
const jwt = require('jsonwebtoken'); // for token auth
const router = express.Router();
router.use(bodyParser.urlencoded({ extended: true }));
router.use(bodyParser.json());

// Auth function for login
module.exports = function (req, res, next){
  
    const token = req.header('token');
    if (!token) return res.status(401).send({ auth: false, message: 'Error: No token provided' }); // empty token

    jwt.verify(token, process.env.TOKEN, function(err, decoded) {
        if (err) return res.status(500).send({ auth: false, message: `Error: Authentication Rejected (Failed to read token data) : token ${err}` }); // wrong token sent
        
        req.user = decoded; // return payload
        next();
      });
};
