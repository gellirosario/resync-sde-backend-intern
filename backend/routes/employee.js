const express = require('express'); // for creating API
const router = express.Router();

// Model used
var Employee = require('../models/Employee');

// Authentication
const auth = require('./auth');

// Get list of all employees
// Test link: http://localhost:3000/getEmployees
router.get('/getEmployees', auth, async(req, res)=>{
    try{
        const result = await Employee.find().sort({e_id:1}); // find all employees (sorted)
        
        if (result){
            //let empNameList = result.map((x)=>{return x.employeeName}); // return names
            res.status(200).send(result); // return data
        }
        else {
            return res.status(404).send('Error: No employees found'); // return error msg
        }
    }
    catch(err){
        return res.status(500).send('Error: Error on the server'); // return error msg
    }
});


// Get details of specific employee
// Required param: e_id (employeeId)
// Test link: http://localhost:3000/getEmployee/2
router.get('/getEmployee/:e_id', auth, async(req, res)=>{

    console.log('Request Id:', req.params.e_id);

    try{
        const result = await Employee.findOne({e_id: req.params.e_id}); // find specific employee with employee id

        if (result){
                res.status(200).send(result); // return data
        }
        else {
            return res.status(404).send(`Error: No employee found with the id of ${req.params.e_id}`); // return error msg
        }
    }
    catch(err){
        return res.status(500).send('Error: Error on the server'); // return error msg
    }
});

// Register new employee (for testing)
// Test link: http://localhost:3000/employee/register
// Param: (Example) 
// {"e_id":"3",
// "employeeName":"Simon Tan"}
router.post('/employee/register',auth, async(req,res)=>{

    // Check if employee exist
    const isExist = await Employee.findOne({e_id: req.body.e_id});
    if (isExist) return res.status(400).send("Error: Employee already exist in database!");

    // Create new employee
    const employee = new Employee({
        e_id: req.body.e_id,
        employeeName: req.body.employeeName,
    });

    try{
        // Add to database
        const emp = await employee.save();
        res.send({user: emp.id}); // return employee id
    }
    catch(err){
        res.status(400).send(err);
    }

});

module.exports = router