const mongoose = require('mongoose'); // connect to mongodb

//Database schema for Employee class
const EmployeeSchema = mongoose.Schema({
    e_id:{
        type: String,
        required: true
    },
    employeeName:{
        type: String,
        required: true
    },
    createdDT: {
        type: Date,
        default: Date.now()
    },
    updatedDT: {
        type: Date,
        default: Date.now()
    }
})

module.exports = mongoose.model('Employee', EmployeeSchema) // model schema to db