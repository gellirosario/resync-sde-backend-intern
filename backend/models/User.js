const mongoose = require('mongoose'); // connect to mongodb

//Database schema for Employee class
const UserSchema = mongoose.Schema({
    username:{
        type: String,
        required: true,
        min: 6,
        max: 255
    },
    password:{
        type: String,
        required: true,
        min: 8,
        max: 1024
    },
    createdDT: {
        type: Date,
        default: Date.now()
    },
    updatedDT: {
        type: Date,
        default: Date.now()
    },
    last_seen: {
        type: Date
    }
})

module.exports = mongoose.model('User', UserSchema) // model schema to db